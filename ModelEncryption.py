import sys
import os
sys.path.append("C:\\Program Files\\DIgSILENT\\PowerFactory 2017 SP8\\Python\\3.6")
sys.path.append("C:\\Users\\Christos.Nikolakakos\\powerfactory-api") # Use your directory for PowerFactory API.
import powerfactory as pf 
from tqdm import tqdm

app=pf.GetApplication()
#app.Show()
print("WARNING: THIS IS AN IRREVERSIBLE PROCESS. PLEASE KEEP BACKUP OF THE PROJECT.")
project = input('Project Name: ')
app.ActivateProject(project)
SV=app.GetCalcRelevantObjects("SV_*.Blkdef")
for blkdef in tqdm(SV):
    if blkdef.loc_name == "SV_Frame_SV_EMT":
        graph=blkdef.SearchObject("SV_EMT_Frame(1).IntGrfnet")
        deleted=graph.Delete()
        if deleted == 0:
            #print(' Frame Graphic Successfully Deleted.')
    else:
        macro=blkdef.PackAsMacro()
        encrypt=blkdef.Encrypt()
        if macro==0:
            pass
            #print(blkdef.loc_name," successfully packed as macro")
        else:
            #print(blkdef.loc_name," could not be packed as macro")
            pass
        if encrypt==0:
            #print(blkdef.loc_name," successfully encrypted")
            pass
        else:
            #print(blkdef.loc_name," could not be encrypted")
            pass
            
        
print("Done!")

            